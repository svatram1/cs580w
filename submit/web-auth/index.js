'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const split = require('split-object');

const fs = require('fs');
const https = require('https');

const userdb = require('./userdb');
const options = require('./options').options;

const cookieParser = require('cookie-parser');
const mustache = require('mustache');
const process = require('process');

const regex_email = /^[a-zA-Z0-9-_.]+@[a-zA-Z]+[.]+[a-zA-Z.]+$/;
const regex_name = /^[a-zA-Z ]*[a-zA-Z]+[a-zA-Z ]*$/;
const regex_pwd = /^[a-zA-Z0-9~!@#$^*()_+=:[\]{}|,.-]*[0-9]+[a-zA-Z0-9~!@#$%^*()_+=:[\]{}|,.-]*$/;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var status = false;
var EMAIL_COOKIE = 'emailId';
const STATIC_DIR = 'statics';
const TEMPLATES_DIR = 'templates';

function setupRoutes(app) {
    app.get('/', rootRedirectToaccount(app));
    app.get('/login.html',getLoginPage(app));
    app.post('/login',userLoginHandler(app));
    app.get('/register',registrationUser(app));
    app.post('/registration',registerUser(app));
    app.get('/account',rootRedirectHandler(app));
    app.post('/logout',logoutHandler(app));
}

function logoutHandler(app) {
    return function(req,res) {
        res.clearCookie(EMAIL_COOKIE);
        status = false;
        res.redirect('/');
    }
}

function rootRedirectToaccount(app) {
    return function(req,res) {
        const getCookie = req.cookies;
        const cookieArr = split(getCookie);
        if(cookieArr[0] === undefined && status === false) {
            res.redirect('/login.html');
        }
        else
        res.redirect('/account');
    }
}

function registrationUser(app) {
    return function(req,res) {
        const getCookie = req.cookies;
        const cookieArr = split(getCookie);
        if(cookieArr[0] === undefined && status === false) {
            res.send(doMustache(app, 'register', {}));
        }
        else
            res.redirect('/account');
    };
}

function getLoginPage(app) {
    return function(req,res) {
        const getCookie = req.cookies;
        const cookieArr = split(getCookie);
        if(cookieArr[0] === undefined && status === false) {
            res.send(doMustache(app, 'login', {}));
        }
        else
            res.redirect('/account');
    }
}


function rootRedirectHandler(app) {
    return function(req,res) {

        const getCookie = req.cookies;
        const cookieArr = split(getCookie);

        if(cookieArr[0] === undefined) {
            res.redirect('/login.html');
        }
        else {
            app.userdb.getAccount(cookieArr[0].key,cookieArr[0].value)
                .then((data) =>  {
                    if(typeof data === 'undefined') {
                        res.clearCookie(cookieArr[0].key);
                        const view = {q3Error: 'OOPS!! Web Service Unavailable'};
                        res.send(doMustache(app,'login',view));

                    }
                    else if(data === 401 || data === 404){
                        res.clearCookie(cookieArr[0].key);
                        status = false;
                        res.redirect('/login.html');
                    }
                    else {
                        const firstname = data.FirstName;
                        const lastname = data.LastName;
                        const view = { first_name: firstname,
                                last_name: lastname };
                        status = true;
                        res.send(doMustache(app,'account',view));
                    }
                }).catch((err) => console.error(err));
        }
    };
}

function registerUser(app) {
    return function (req,res) {

            const email = req.body.email;
            const firstname = req.body.firstname;
            const lastname = req.body.lastname;

            var test_email = regex_email.test(email);
            var test_name1 = regex_name.test(firstname);
            var test_name2 = regex_name.test(lastname);

            const pwd = req.body.pwd;
            var test_pwd = regex_pwd.test(pwd);
            const cnf_pwd = req.body.check_pwd;

            if (typeof firstname === 'undefined' || firstname.trim().length === 0 || test_name1 != true) {
            const page_view = {q1Error: 'Please provide a valid name',
                                firstname: firstname, lastname: lastname, email: email};
            res.send(doMustache(app, 'register',page_view));
            }

            else if (typeof lastname === 'undefined' || lastname.trim().length === 0 || test_name2 != true) {
            const page_view = {q2Error: 'Please provide a valid name',
                                firstname: firstname, lastname: lastname, email: email};
            res.send(doMustache(app, 'register', page_view));
            }

            else if (typeof email === 'undefined' || email.trim().length === 0 || test_email != true) {
                const page_view = {
                    q3Error: 'Please provide a valid email address like user@example.[com | co.in | edu etc]',
                    firstname: firstname, lastname: lastname, email: email
                };
                res.send(doMustache(app, 'register', page_view));
            }
            else if(typeof pwd === 'undefined'|| pwd.trim().length < 8 || test_pwd != true) {
            const page_view = {q4Error: 'Plese enter a valid password',
                                q6Error: 'Should be atleast 8 characters',
                                q7Error: 'Should contain atleast one digit',
                                q8Error: 'Should contain whitespaces',
                                q9Error: 'May contain special characters ! @ # $ % ^ & * ( ) + = _ :',
                                firstname: firstname, lastname: lastname, email: email};
            res.send(doMustache(app, 'register', page_view));
            }

            else if (typeof cnf_pwd === 'undefined' || cnf_pwd.trim().length === 0) {
            const page_view = {q5Error: 'Please enter the valid password',
                firstname: firstname, lastname: lastname, email: email};
            res.send(doMustache(app, 'register', page_view));
            }

            else if(pwd != cnf_pwd) {
            const page_view = {q4Error: 'Passwords should be matched',
                firstname: firstname, lastname: lastname, email: email};
            res.send(doMustache(app, 'register', page_view));
            }
            else {
            app.userdb.addUser(email, firstname, lastname, pwd)
                .then(function (response) {
                    if(typeof response === 'undefined') {
                        const errors = {qError: 'OOPS!! Web Service Unavailable',
                            firstname: firstname, lastname: lastname, email: email};
                        res.send(doMustache(app, 'register', errors));
                    }
                    else if(response === 303) {
                        const errors = {q3Error: 'This email ID is already registered',
                            firstname: firstname, lastname: lastname, email: email};
                        res.send(doMustache(app, 'register', errors));
                    }
                    else {
                        EMAIL_COOKIE = email;

                        res.cookie(EMAIL_COOKIE, response);

                        status = true;
                        res.redirect('/account');
                    }
                }).catch((err) => console.error(err));
        }
    }
}

function userLoginHandler(app) {
    return function(req,res) {
         {
            const email = req.body.email;
            const pwd = req.body.pwd;

            var html = null;
            var test_email = regex_email.test(email);
            if (typeof email === 'undefined' || email.trim().length === 0 || test_email === false) {
                const errors = {qError: 'Please provide a valid emailId', email: email};
                html = doMustache(app, 'login', errors);
                res.send(html);
            }
            else if (typeof pwd === 'undefined' || pwd.trim().length === 0) {
                const errors = {q1Error: 'Please enter the password', email:email};
                html = doMustache(app, 'login', errors);
                res.send(html);
            }
            else {
                app.userdb.search(email, pwd)
                    .then((json) => {
                        var errors = null;
                        if(json === undefined) {
                            errors = {
                                q3Error: 'OOPS!! Web Service Unavailable',
                                email: email
                            }
                            html = doMustache(app, 'login', errors);
                            res.send(html);
                        }
                        else if (json === 401) {
                            errors = {
                                q1Error: 'Incorrect Password',
                                email: email
                            };
                            html = doMustache(app, 'login', errors);
                            res.send(html);
                        }
                        else if (json === 404) {
                            errors = {
                                q3Error: 'No account is registered with this email id',
                                email: email
                            };
                            html = doMustache(app, 'login', errors);
                            res.send(html);
                        }
                        else {
                            const token = json.authToken;
                            app.userdb.getAccount(email, token)
                                .then((data) => {
                                    EMAIL_COOKIE = email;
                                    res.cookie(EMAIL_COOKIE, token);
                                    status = true;
                                    res.redirect('/account');
                                }).catch((err) => console.error(err));
                        }
                    })
                    .catch((err) => console.error(err));
            }
        }
    }
}

function doMustache(app, templateId, view) {
    return mustache.render(app.templates[templateId], view);
}

function setupTemplates(app) {
    app.templates = {};
    for (let fname of fs.readdirSync(TEMPLATES_DIR)) {
        const m = fname.match(/^([\w\-]+)\.ms$/);
        if (!m) continue;
        try {
            app.templates[m[1]] =
                String(fs.readFileSync(`${TEMPLATES_DIR}/${fname}`));
        }
        catch (e) {
            console.error(`cannot read ${fname}: ${e}`);
            process.exit(1);
        }
    }
}


function setup() {
    const app = express();
    const port = options.port;
    setupTemplates(app);
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(cookieParser());
    app.userdb = userdb;
    app.use(express.static(STATIC_DIR));
    setupRoutes(app);
    https.createServer({
        key: fs.readFileSync(`${options.sslDir}/key.pem`),
        cert: fs.readFileSync(`${options.sslDir}/cert.pem`),
    }, app).listen(port, function() {
        console.log(`listening on port ${port}`);
    });
}

setup();

module.exports = {
    setup: setup
};
