'use strict';

const axios = require('axios');

const options = require('./options').options;

const WS_URL = options.ws_url;

function Userdb() {
    this.baseUrl = WS_URL;
}

Userdb.prototype.search = function(email,pwd) {
    const data = Object.assign({}, {pw: pwd});
    return axios.put(`${this.baseUrl}/users/${email}/auth`,data,{maxRedirects:0})
        .then((response) => {
        return response.data;
        }).catch((err) => {
            if(err.response)
            return err.response.status;
            else
                return err.response;
        });
}


Userdb.prototype.addUser = function(email,firstname,lastname,pwd) {
    const data = Object.assign({}, {FirstName: firstname, LastName: lastname, EmailId: email});
    var cookie;
    return axios.put(`${this.baseUrl}/users/${email}?pw=${pwd}`,data, {maxRedirects: 0})
        .then((response) => {
        cookie = response.data.authToken;
        return cookie;
    }).catch((err) => {
        if(err.response)
        return err.response.status;
        else
            return err.response;
    });
}

Userdb.prototype.getAccount = function(email,authToken) {
    return axios.get(`${this.baseUrl}/users/${email}`,{headers: {authorization: `Bearer ${authToken}`}})
        .then((response) => {
            return response.data;
        }).catch((err) => {
            if(err.response)
                return err.response.status;
            else
                return err.response;
    });
}

module.exports = new Userdb();

