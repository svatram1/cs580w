<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <link rel="stylesheet" href="/css/style.css">
    </head>
<body>
<h1 align="center">Login Page</h1>
<p class="error">{{msg}}</p>
<form id="form_login" method="POST" action="/login">
    <label>
        {{#q3Error}}<span class="error">{{q3Error}}<br></span>{{/q3Error}}
        <input align="center" type = "text" placeholder="Email ID" name="email" class="control" value="{{email}}"><br>
        {{#qError}}<span class="error">{{qError}}<br></span>{{/qError}}
    </label>
    <br>
    <label>
        <input align="center" type = "password" placeholder="Password" name="pwd" class="control" value="{{pwd}}"><br>
        {{#q1Error}}<span class="error">{{q1Error}}<br></span>{{/q1Error}}
    </label>
    <br>
    <input align="center" name="submit" type="submit" value="Log In" class="control">
    <br>
    <p class="footer">Not Registered? <a href="/register">Create an account</a></p>
</form>
</body>
</html>

