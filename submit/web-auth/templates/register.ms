<!DOCTYPE html>
<html>
<head>
    <title>Register</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<p>
<h1 align = "center">Registration page</h1>
<form id="form_register" method="POST" action="/registration">
    <label>
        {{#qError}}<span class="error">{{qError}}<br></span>{{/qError}}
        <span class="label">First Name</span>
        <input name="firstname" class="control" value="{{firstname}}"><br>
        {{#q1Error}}<span class="error">{{q1Error}}<br></span>{{/q1Error}}
    </label>
    <br>
    <label>
        <span class="label">Last Name</span>
        <input name="lastname" class="control" value="{{lastname}}"><br>
        {{#q2Error}}<span class="error">{{q2Error}}<br></span>{{/q2Error}}
    </label>
    <br>
    <label>
        <span class="label">Email Address</span>
        <input name="email" class="control" value="{{email}}"><br>
        {{#q3Error}}<span class="error">{{q3Error}}<br></span>{{/q3Error}}
    </label>
    <br>
    <label>
        <span class="label">Enter a Password</span>
        <input type= "password" name="pwd" class="control" value="{{pwd}}"><br>
        {{#q4Error}}<span class="error">{{q4Error}}<br></span>{{/q4Error}}
        {{#q6Error}}<span class="error">{{q6Error}}<br></span>{{/q6Error}}
        {{#q7Error}}<span class="error">{{q7Error}}<br></span>{{/q7Error}}
        {{#q8Error}}<span class="error">{{q8Error}}<br></span>{{/q8Error}}
        {{#q9Error}}<span class="error">{{q9Error}}<br></span>{{/q9Error}}
    </label>
    <br>
    <label>
        <span class="label">Confirm Password</span>
        <input type= "password" name="check_pwd" class="control" value="{{check_pwd}}"><br>
        {{#q5Error}}<span class="error">{{q5Error}}<br></span>{{/q5Error}}
    </label>
    <br>
    <input name="submit" type="submit" value="Register" class="control">
    <br>
    <p class="footer">Already registered? <a href="/login.html">Log In</a></p>
</form>
</p>
</body>
</html>




