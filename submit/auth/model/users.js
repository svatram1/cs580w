const assert = require('assert');
const USERS = 'users';

function Users(db) {
    this.db = db;
    this.users = db.collection(USERS);
}

Users.prototype.getUser = function(id) {
    const query = {"_id":id};
    return this.users.find(query).toArray();
}

Users.prototype.newUser = function(id,pw,user,time_period,token) {
        const d = {_id: id, data: user, password: pw, auth_time: [time_period], auth_tokens: [token]};
        return this.users.insertOne(d).then(function (results) {
            return new Promise((resolve) => resolve(results.insertedId));
        });

}

Users.prototype.updateUser = function(id,time_period,token) {
    this.users.update({"_id": id}, {$push:{"auth_time": time_period, "auth_tokens": token}});
    setTimeout(() => { },500);
}

Users.prototype.finalUser = function(token_valid,id,auth_token,time) {
    if (!token_valid) {
        this.users.update({_id:id}, {$pull: {auth_tokens: auth_token, auth_time: time}});
    }
    const query = {"_id": id, "auth_tokens": auth_token};
    return this.users.find(query, {_id: false, data: true}).toArray();
}

module.exports = {
    Users: Users
};
