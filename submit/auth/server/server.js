const express = require('express');
const bodyParser = require('body-parser');
const randomstring = require('randomstring');
const https = require('https');
const fs = require('fs');
var path = require('path');
var bcrypt = require('bcrypt');
const saltRounds = 10;

const OK = 200;
const CREATED = 201;
const BAD_REQUEST = 400;
const UNAUTHORIZED = 401;
const NOT_FOUND = 404;
const SERVER_ERROR = 500;
const SEE_OTHER = 303;
const NO_CONTENT = 204;
var auth_Time = 0;
var dir_name = '.';
var auth_token_valid =true;
var i=0,time=0;

function serve(port,authTime,dir, model) {
    const app = express();
    auth_Time = authTime;
    dir_name = dir;
    app.locals.model = model;
    app.locals.port = port;
    setupRoutes(app);

    const KEY_PATH = dir_name;
    const CERT_PATH = dir_name;

    https.createServer({
        key: fs.readFileSync(KEY_PATH + '/key.pem'),
        cert: fs.readFileSync(CERT_PATH + '/cert.pem'),
    },app).listen(port, function() {
        console.log(`listening on port ${port}`);
    });
}

function setupRoutes(app) {
    app.use('/users/:id', bodyParser.json());
    app.get('/users/:id', getUser(app));
    app.put('/users/:id', putNewUser(app));
    app.put('/users/:id/auth', putUser(app));
}

module.exports = {
    serve: serve
}

function requestUrl(req) {
    const port = req.app.locals.port;
    return `${req.protocol}://${req.hostname}:${port}${req.originalUrl}`;
}


function putNewUser(app) {
    return function (request, response) {
	const id = request.params.id;
      	const pw = request.query.pw;
        if (typeof pw === 'undefined' || typeof pw !== 'string' || !pw) {
            response.status(BAD_REQUEST).send(`Error: /users/${id} requires a valid 'pw' password query parameter`);
        }
        else {
            var salt = bcrypt.genSaltSync(saltRounds);
            var hashed_pw = bcrypt.hashSync(pw, salt);
        const userInfo = request.body;
            request.app.locals.model.users.getUser(id).then(function (results) {
                if (results.length != 0) {
                    response.append('Location', requestUrl(request));
                    response.status(SEE_OTHER).send({
                            "status": "EXISTS",
                            "info": `user ${id} already exists`
                        });
                }
                else {
                    var auth_token = randomstring.generate(8);
                    var req_time = Date.now();
                    var time_period = (auth_Time * 1000) + req_time;
                    request.app.locals.model.users.newUser(id, hashed_pw, userInfo, time_period, auth_token).then(function (id) {
                        response.append('Location', requestUrl(request));
                        response.status(CREATED).send({
                            "status": "CREATED",
                            "authToken": `${auth_token}`
                        });
                    }).catch((err) => {
                        console.error(err);
                    response.sendStatus(SERVER_ERROR);

                    });
                }
            });
        }
    }
}

function putUser(app) {
    return function(request, response) {
        const id = request.params.id;
        const body = request.body.pw;
        if(typeof id === 'undefined') {
            response.sendStatus(BAD_REQUEST);
        }

        else {
            request.app.locals.model.users.getUser(id).then(function(results) {
                if(results.length!=0) {

			if(typeof body === 'undefined' || typeof body === Number) {
            			response.status(UNAUTHORIZED).send({ "status": "ERROR_UNAUTHORIZED",
                		"info": `/users/${id}/auth requires a valid 'pw' password query parameter`
            			});
			}

                    if (!bcrypt.compareSync(body,results[0].password)) {
                        response.status(UNAUTHORIZED).send({
                            "status": "ERROR_UNAUTHORIZED",
                            "info": `/users/${id}/auth requires a valid 'pw' password query parameter`
                        });
                    }
                    else {
                        var auth_token = randomstring.generate(8);
                        var req_time = Date.now();
                        var time_period = (auth_Time * 1000) + req_time;
                        response.status(OK).send({
                            "status": "OK",
                            "authToken": `${auth_token}`
                        });

                        request.app.locals.model.users.updateUser(id, time_period, auth_token);

                    }
                }
                else {
                    response.status(NOT_FOUND).send({
                        "status": "ERROR_NOT_FOUND",
                        "info": `user ${id} not found`
                    });
                }

            }).catch((err) => {
                console.error(err);
                response.sendStatus();
            });
        }
    }
}

function getUser(app) {
    return function (request, response) {
        var req_time = Date.now();
        const id = request.params.id;
        var time_period = req_time;
        const auth_header = request.headers.authorization;

        request.app.locals.model.users.getUser(id).then(function (result) {
        if (result.length === 0) {
            response.status(NOT_FOUND).send({
                        		"status": "ERROR_NOT_FOUND",
                        		"info": `user ${id} not found`
                    			});
        }

	else {

		const auth_header = request.headers.authorization;
        	if (!auth_header) {
            	response.status(401).send({
                			"status": "ERROR_UNAUTHORIZED",
                			"info": `/users/${id} 1 requires a bearer authorization header`
            				});
        	}
        	else {
            	const auth_token = auth_header.split(' ')[1];
                    for (i = 0; i < result[0].auth_tokens.length; i++) {
                        if (result[0].auth_tokens[i] === auth_token) {
                            if (result[0].auth_time[i] < time_period) {
				console.log("valid");
                                auth_token_valid = false;
                                time = result[0].auth_time[i];
                            }
                        }
                    }
                    request.app.locals.model.users.finalUser(auth_token_valid, id, auth_token, time).then(function (results) {
                        if (results.length != 0) {
                            response.status(OK).send(results);
                        }
                        else {
                            response.status(401).send({
                                "status": "ERROR_UNAUTHORIZED",
                                "info": `/users/${id} 2 requires a bearer authorization header`
                            });
                        }
                    }).catch((err) => {
                        console.error(err);
                    response.sendStatus();
                });
                }
	}

            }).catch((err) => {
                console.error(err);
            response.sendStatus();
        });
        }
    }


function requestUrl(req) {
    const port = req.app.locals.port;
    return `${req.protocol}://${req.hostname}:${port}${req.originalUrl}`;
}
