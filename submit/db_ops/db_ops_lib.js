'use strict';

const assert = require('assert');
const mongo = require('mongodb').MongoClient;


//used to build a mapper function for the update op.  Returns a
//function F = arg => body.  Subsequently, the invocation,
//F.call(null, value) can be used to map value to its updated value.
function newMapper(arg, body) {

  return new (Function.prototype.bind.call(Function, Function, arg, body));
}

//print msg on stderr and exit.
function error(msg) {
  console.error(msg);
  process.exit(1);
}

//export error() so that it can be used externally.
module.exports.error = error;


//auxiliary functions; break up your code into small functions with
//well-defined responsibilities.



//perform op on mongo db specified by url.

function dbOp(url, op) {

  //your code goes here

let c = JSON.parse(op);

var i;

mongo.connect(url, function(err,db)
{

	if(c["op"] === "create")
	{
		var create = db.collection(c["collection"]).insertMany(c["args"], function(err,r)
		{
			if (err) throw err;
		});

	}


	else if(c["op"] === "read")
	{

		var read = db.collection(c["collection"]).find(c["args"]).toArray(function(err, db1)
		{
			if (err) throw err;

			for(i=0;i<db1.length;i++)
			{
				console.log(db1[i]);
			}

		});

	}


	else if(c["op"] === "delete")
	{

		var del = db.collection(c["collection"]).deleteMany(c["args"], function(err, d)
		{
			if (err) throw err;
		});
	}


	else if(c["op"] === "update")
	{
		var mapper = newMapper(c["fn"][0],c["fn"][1]);

		db.collection(c["collection"]).find(c["args"]).toArray(function(err,db2)
		{
			if (err) throw err;

			var n = [];

		for(i=0;i<db2.length;i++)
		{
			n[i] = mapper.call(null,db2[i]);

			db.collection(c["collection"]).updateOne(db2[i],n[i],function(err,db3)
			{

			if (err) throw err;

			});

		}

		});


	}


setTimeout(() => db.close(),1000);

});

}

//make main dbOp() function available externally

module.exports.dbOp = dbOp;

