const express = require('express');
const bodyParser = require('body-parser');

const OK = 200;
const CREATED = 201;
const BAD_REQUEST = 400;
const NOT_FOUND = 404;
const SERVER_ERROR = 500;
const SEE_OTHER = 303;
const NO_CONTENT = 204;

function serve(port, model) {
    const app = express();
    app.locals.model = model;
    app.locals.port = port;
    setupRoutes(app);
    app.listen(port, function() {
        console.log(`listening on port ${port}`);
    });
}

function setupRoutes(app) {
    app.get('/users/:ID', getUsers(app));
    app.use('/users/:ID', bodyParser.json());
    app.put('/users/:ID', newUser(app));
    app.delete('/users/:ID', deleteUser(app));
    app.post('/users/:ID',updateUSer(app));
}

function requestUrl(req) {
    const port = req.app.locals.port;
    return `${req.protocol}://${req.hostname}:${port}${req.originalUrl}`;
}

module.exports = {
    serve: serve
}

function getUsers(app) {
    return function(request, response) {
        const q = request.params.ID;
        if (typeof q === 'undefined') {
            response.sendStatus(BAD_REQUEST);
        }
        else {
            request.app.locals.model.users.getUser(q).
            then(function (results) {
                if(results.length!=0)
                    response.json(results)
                else
                    response.sendStatus(NOT_FOUND)
            }).
            catch((err) => {
                console.error(err);
            response.sendStatus(SERVER_ERROR);
        });
        }
    };
}

function newUser(app) {

    return function(request, response) {
        const body = request.body;
        body.my_user_id1 = request.params.ID;
        const q = request.params.ID;

        request.app.locals.model.users.getUser(q).
        then(function (results) {
            if(results.length!=0) {
                request.app.locals.model.users.replaceUser(q, body).then(function (body) {
                    response.sendStatus(NO_CONTENT);
                }).
                catch((err) => {
                    console.error(err);
                response.sendStatus(NOT_FOUND);
            });

            }
            else
            {
                request.app.locals.model.users.newUser(body).
                then(function(body) {
                    response.append('Location', requestUrl(request));
                    response.sendStatus(CREATED);
                }).
                catch((err) => {
                    console.error(err);
                response.sendStatus(SERVER_ERROR);
            });

            }
        });
    }
}

function deleteUser(app) {
    return function(request, response) {
        const id = request.params.ID;
        if (typeof id === 'undefined') {
            response.sendStatus(BAD_REQUEST);
        }
        else {
            request.app.locals.model.users.deleteUser(id).
            then(() => response.end()).
            catch((err) => {
                console.error(err);
            response.sendStatus(NOT_FOUND);
        });
        }
    };
}

function updateUSer(app) {
    return function(request, response) {
        const q = request.params.ID;
        const body = request.body;
        body.my_user_id1 = request.params.ID;
        if (typeof q === 'undefined') {
            response.sendStatus(BAD_REQUEST);
        }
        else {
            request.app.locals.model.users.replaceUser(q, body).
            then(function (body) {
                response.append('Location', requestUrl(request));
                response.sendStatus(SEE_OTHER);
            }).
            catch((err) => {
                console.error(err);
            response.sendStatus(NOT_FOUND);
        });
        }
    };
}

