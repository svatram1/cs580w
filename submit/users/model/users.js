const assert = require('assert');
const ObjectID = require('mongodb').ObjectID;

const USERS = 'users';

function Users(db) {
    this.db = db;
    this.users = db.collection(USERS);
}

Users.prototype.newUser = function(ID) {
    return this.users.insertOne(ID).
    then(function(results) {
        return new Promise((resolve) => resolve(results.insertedId));
    });
}

Users.prototype.getUser = function(params) {
    const query = {"my_user_id1": params};
    return this.users.find(query,{my_user_id1:false}).toArray();
}

Users.prototype.deleteUser = function(id) {
    var query = {"my_user_id1": id};
    return this.users.deleteOne(query).
    then(function(results) {
        return new Promise(function(resolve, reject) {
            if (results.deletedCount === 1) {
                resolve();
            }
            else {
                reject(new Error(`cannot delete user ${id}`));
            }
        });
    });
}

Users.prototype.replaceUser = function(id,body) {
    var query = {"my_user_id1": id};
            return this.users.updateOne(query,body).then(function (results) {
                    return new Promise(function (resolve, reject) {
                        if (results.modifiedCount === 1) {
                            resolve();
                        }
                        else {
                            reject(new Error(`User ID not found`));
                        }
                    });
            });
}

module.exports = {
    Users: Users
};

